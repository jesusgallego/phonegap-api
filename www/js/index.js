/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

 function checkConnection(networkState) {
     var states = {};
     states[Connection.UNKNOWN]  = 'Tipo de conexión desconocida';
     states[Connection.ETHERNET] = 'Ethernet';
     states[Connection.WIFI]     = 'WiFi';
     states[Connection.CELL_2G]  = 'Cell 2G';
     states[Connection.CELL_3G]  = 'Cell 3G';
     states[Connection.CELL_4G]  = 'Cell 4G';
     states[Connection.CELL]     = 'Cell generic';
     states[Connection.NONE]     = 'Sin conexión';

     return states[networkState];
 }

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("online", function () {app.newLog("Se ha conectado a la red")}, false);
        document.addEventListener("offline", function () {app.newLog("Se ha perdido la conexión")}, false);
        document.addEventListener('pause', function(){ app.newLog("App en Pausa")}, false);
        document.addEventListener('resume', function(){ app.newLog("App Reanudada")}, false);
        document.addEventListener('backbutton', function(){ app.newLog("Botón de atrás")}, false);
        document.addEventListener('volumedownbutton', function(){ app.newLog("Bajar volumen")}, false);
        document.addEventListener('volumeupbutton', function(){ app.newLog("Subir volumen")}, false);
        document.addEventListener('menubutton', function(){ app.newLog("Botón de menú")}, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        app.newLog("API de PhoneGap cargada.");
        app.pluginsInfo()
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    pluginsInfo: function() {
      var version = document.getElementById('version');
      var model = document.getElementById('model');
      var platform = document.getElementById('platform');
      var uuid = document.getElementById('uuid');
      var android = document.getElementById('android');
      var connection = document.getElementById('connection');
      var orientation = document.getElementById('orientation');
      var accelX = document.getElementById('accel-x');
      var accelY = document.getElementById('accel-y');
      var accelZ = document.getElementById('accel-z');
      var language = document.getElementById('language');

      version.innerHTML = device.cordova;
      model.innerHTML = device.model;
      platform.innerHTML = device.platform;
      uuid.innerHTML = device.uuid;
      android.innerHTML = device.version;
      connection.innerHTML = checkConnection(navigator.connection.type);

      navigator.compass.watchHeading(function(heading) {
          orientation.innerHTML = heading.magneticHeading + "º";
      },
      function (error) {
        alert('Compass error: ' + error.code)
      },
      {
        frequency: 3000
      });

      navigator.accelerometer.watchAcceleration(function(acceleration) {
        accelX.innerHTML = acceleration.x;
        accelY.innerHTML = acceleration.y;
        accelZ.innerHTML = acceleration.z;
      },
      function (error) {
        alert('accelerometer error: ' + error)
      },
      {
        frequency: 3000
      });

      navigator.globalization.getPreferredLanguage(function(lang) {
        language.innerHTML = lang.value
      },
      function () {
        alert('Error getting language')
      });
    },
    newLog: function(message) {
      var log = document.getElementById('log');
      log.innerHTML += "- " + message + "<br/>";
    }
};
